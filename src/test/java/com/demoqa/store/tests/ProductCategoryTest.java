package com.demoqa.store.tests;

import com.data.Contact;
import com.demoqa.store.pageobjects.HomePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProductCategoryTest extends TestBase {
    protected final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);
    @Test
    public void addMagicMouseToCartAndCheckout() {
        homepage = new HomePage(wd);
        final String accessoryTitle = "Magic Mouse";
        accessoriesPage = homepage.selectAccessories();
        checkoutPage = accessoriesPage.addItemAndCheckout(accessoryTitle);
        Assert.assertEquals(checkoutPage.getQuantity(accessoryTitle), "1","Verify quantity of product on Checkout page");
    }
    @Test (groups = {"endtoend"})
    public void addMagicMouseAndCompleteTransaction() {
        homepage = new HomePage(wd);
        final String accessoryTitle = "Magic Mouse";
        accessoriesPage = homepage.selectAccessories();
        checkoutPage = accessoriesPage.addItemAndCheckout(accessoryTitle);
        Assert.assertEquals(checkoutPage.getQuantity(accessoryTitle), "1","Verify quantity of product on Checkout page");
        checkoutPage.fillbillingContactAndPurchase(new Contact());
        transactionResultsPage = checkoutPage.purchase();
        Assert.assertTrue(transactionResultsPage.containsPurchasedItem(accessoryTitle), "Verify the product is payed for");
    }
}
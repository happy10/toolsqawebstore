package com.demoqa.store.tests;

import com.common.Driver;
import com.common.PropertiesUtil;
import com.demoqa.store.pageobjects.AccessoriesPage;
import com.demoqa.store.pageobjects.CheckoutPage;
import com.demoqa.store.pageobjects.HomePage;
import com.demoqa.store.pageobjects.TransactionResultsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

/**
 * Created by olhack on 2018-04-21.
 */
public class TestBase {
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    WebDriver wd;
    HomePage homepage;
    AccessoriesPage accessoriesPage;
    CheckoutPage checkoutPage;
    TransactionResultsPage transactionResultsPage;

    PropertiesUtil propertyUtil = new PropertiesUtil();

    @BeforeClass
    public void beforeClass(){
        String browser = propertyUtil.getTestConfigProperty("browser");
        String OS = System.getProperty("os.name").toLowerCase();
        String platformOs = propertyUtil.getTestConfigProperty("platformOs") == null ? OS : propertyUtil.getTestConfigProperty("platformOs");
        String driver = "";
        if (OS.indexOf("win") >= 0) {
            platformOs = "win";
            if ("chrome".equals(browser)){
                driver = Driver.WinOS.chromedriver();
            } else if ("firefox".equals(browser)) {
                driver = Driver.WinOS.geckodriver();
            } else if ("ie".equals(browser)) {
                driver = Driver.WinOS.iedriver();
            } else if ("edge".equals(browser)) {
                driver = Driver.WinOS.edgedriver();
            }
        } else if (OS.indexOf("mac") >= 0) {
            platformOs = "osx";
            if ("chrome".equals(browser)){
                driver = Driver.MacOS.chromedriver();
            } else if ("firefox".equals(browser)) {
                driver = Driver.MacOS.geckodriver();
            } else if ("safari".equals(browser)) {
                driver = Driver.MacOS.safari();
            }
        } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 ) {
            platformOs = "linux";
        }

        String driverPathByOS = String.format("src/test/resources/browsers/%s/%s/%s",browser, platformOs, driver);
        LOGGER.info("OS detected: {}", OS);
        LOGGER.info("Browser: {}", browser);
        LOGGER.info("Driver: {}", driverPathByOS);

        System.setProperty("webdriver.chrome.driver", driverPathByOS);
        System.setProperty("webdriver.gecko.driver", driverPathByOS);
        System.setProperty("webdriver.ie.driver", driverPathByOS);
        System.setProperty("webdriver.edge.driver", driverPathByOS);

        if (driver.indexOf("chrome") >=0) {
            wd = new ChromeDriver();
        } else if (driver.indexOf("gecko") >=0) {
            wd = new FirefoxDriver();
        } else if (driver.indexOf("ie") >=0) {
            wd = new InternetExplorerDriver();
        } else if (driver.indexOf("edge") >=0) {
            wd = new EdgeDriver();
        } else if (driver.indexOf("safari") >=0) {
            wd = new SafariDriver();
        }
        wd.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        wd.manage().window().maximize();
    }

    @AfterClass
    public void afterClass() {
        wd.quit();
    }
}

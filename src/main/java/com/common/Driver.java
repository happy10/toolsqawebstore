package com.common;

public enum Driver {
    WinOS("chromedriver.exe","geckodriver.exe","iedriver.exe","iedriver.exe", ""),
    MacOS("chromedriver","geckodriver","","","safari"),
    LinuxOS("chromedriver","geckodriver","","","");

    private String chromedriver;
    private String geckodriver;
    private String iedriver;
    private String safaridriver;
    private String edgedriver;

    Driver(String chromedriver, String geckodriver, String iedriver, String edgedriver, String safari){
        this.chromedriver = chromedriver;
        this.geckodriver = geckodriver;
        this.iedriver = iedriver;
        this.safaridriver = safari;
        this.edgedriver = edgedriver;
    }

    public String chromedriver(){return chromedriver;}
    public String geckodriver(){return geckodriver;}
    public String iedriver(){return iedriver;}
    public String safari(){return safaridriver;}
    public String edgedriver() { return edgedriver;}
}

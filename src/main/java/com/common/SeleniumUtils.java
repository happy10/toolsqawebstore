package com.common;

import java.util.concurrent.TimeUnit;

/**
 * Created by olhack on 2018-04-22.
 */
public class SeleniumUtils {
    public static void pause(Integer milliseconds){
        try {
            TimeUnit.MILLISECONDS.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

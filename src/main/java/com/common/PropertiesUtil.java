package com.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesUtil {
    String resourcesPath = System.getProperty("user.dir") + "/src/test/resources/config/".replace("/", File.separator);
    String testConfigPath = resourcesPath + "test_config";

    static Properties testConfigProperties = new Properties();
    static Properties envProperties = new Properties();

    public PropertiesUtil() {
        loadProperties();
    }

    public void loadProperties(){
        try {
            testConfigProperties.load(new FileInputStream(testConfigPath));
        } catch (IOException fe) {
            fe.printStackTrace();
        }
    }

    public static String getEnvProperty(String property){
        return envProperties.getProperty(property);
    }
    public static String getTestConfigProperty(String property){
        return testConfigProperties.getProperty(property);
    }
}

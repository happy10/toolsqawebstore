package com.demoqa.store.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TransactionResultsPage extends PageBase{

	public TransactionResultsPage(WebDriver wd) {
		super(wd);
	}
	public boolean containsPurchasedItem(String accessoryTitle) {
		return elementsOnThePage(By.xpath("//*[@class='wpsc-purchase-log-transaction-results']//*[text()='"+ accessoryTitle +"']")).isEmpty()?false:true;
	}
}

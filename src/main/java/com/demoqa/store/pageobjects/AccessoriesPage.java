/**
 * 
 */
package com.demoqa.store.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author daddy
 *
 */
public class AccessoriesPage extends PageBase{
	PageBase chackoutPage;
	public AccessoriesPage(WebDriver wd){
		super(wd);
		LOGGER.debug("Accessories page loaded");
	}
	private void addProductToCartFromProductPage(){}
	private void addProductToCart(String productTitle, String... quantity){
        WebElement checkoutCountElement = wd.findElement(By.className("count"));
	    try {
            wd.findElement(By.xpath("//*[text()='"+ productTitle +"']//ancestor::div[@class='productcol']//*[@class='input-button-buy']")).click();
            LOGGER.debug("Selecting product {}", productTitle);
        } catch (NoSuchElementException nse) {
	        LOGGER.error("Product \"{}\" was not found", productTitle);
        }
        waitForCheckoutCountChange(checkoutCountElement, quantity.length > 0?quantity[0]:"1");
	}
	
	public void selectGoToCheckout(){
		wd.findElement(ById.xpath("//a[@class='cart_icon']")).click();
	}
	public CheckoutPage addItemAndCheckout(String productTitle){
		addProductToCart(productTitle);
		selectGoToCheckout();
		return new CheckoutPage(wd);
	}
}

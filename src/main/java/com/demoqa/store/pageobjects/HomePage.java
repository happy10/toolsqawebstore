package com.demoqa.store.pageobjects;

import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class HomePage extends PageBase{
	AccessoriesPage assessPage;
	
	public HomePage(WebDriver wd){
		super(wd);
		wd.get("http://store.demoqa.com");
		LOGGER.debug("Home page loaded");
	}
	
	public AccessoriesPage selectAccessories(){
		  Actions action = new Actions(wd);
		  WebElement product = wd.findElement(ById.cssSelector("#menu-item-33>a"));
		  action.moveToElement(product).build().perform();
		  wd.findElement(ById.cssSelector("#menu-item-34>a")).click();
		return new AccessoriesPage(wd);
	}
}

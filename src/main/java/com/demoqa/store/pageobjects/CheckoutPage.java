package com.demoqa.store.pageobjects;

import com.data.Contact;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.WebDriver;

public class CheckoutPage extends PageBase{
	public CheckoutPage(WebDriver wd){
		super(wd);
		LOGGER.debug("Checkout page loaded");;
	}
	public String getQuantity(String productTitle){
		return wd.findElement(By.xpath("//a[text()='"+ productTitle +"']//ancestor::tr//input[@type='text']")).getAttribute("value");
	}
	public String getPrice(){
		return wd.findElement(ById.cssSelector(".pricedisplay")).getText();
	}
	public void continueWithCheckout(){
		wd.findElement(ByCssSelector.cssSelector(".step2>span")).click();
	}
	public void fillbillingContactAndPurchase(Contact contact){
		continueWithCheckout();
		typeWhenReady(contact.getEmail(),ById.cssSelector("#wpsc_checkout_form_9"));
		typeWhenReady(contact.getFirstName(),ById.cssSelector("#wpsc_checkout_form_2"));
		type(contact.getLastName(),ById.cssSelector("#wpsc_checkout_form_3"));
		type(contact.getAddress(),ById.cssSelector("#wpsc_checkout_form_4"));
		type(contact.getCity(),ById.cssSelector("#wpsc_checkout_form_5"));
		typeWhenReady(contact.getProvince(),ById.cssSelector("#wpsc_checkout_form_6"));
		typeWhenReady(contact.getPostalCode(),ById.cssSelector("#wpsc_checkout_form_17"));
		typeWhenReady(contact.getPhone(),ById.cssSelector("#wpsc_checkout_form_18"));
		wd.findElement(ById.xpath(".//*[@id='wpsc_checkout_form_7']/option[41]")).click();
		checkSameShippingAddress();
	}
	private void checkSameShippingAddress() {
		click(ById.cssSelector("#shippingSameBilling"));
	}
	public TransactionResultsPage purchase() {
		click(ById.cssSelector(".make_purchase.wpsc_buy_button"));
		return new TransactionResultsPage(wd);
	}
}

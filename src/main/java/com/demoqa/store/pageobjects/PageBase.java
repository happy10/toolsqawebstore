package com.demoqa.store.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class PageBase {
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	WebDriver wd;
	WebDriverWait wdwait;
	public PageBase(WebDriver wd){
		this.wd=wd;
		this.wdwait = new WebDriverWait(wd, 30);
	}
	
	public String getTitle(){
		return wd.getTitle();
	}

	protected void click(By locator) {
		WebElement element = wdwait.until(ExpectedConditions.elementToBeClickable(locator));
		element.click();
	}

	protected void type(String address, By locator) {
		WebElement element = wd.findElement(locator);
		element.clear();
		element.sendKeys(address);
	}
	protected void typeWhenReady(String address, By locator) {
		WebElement element = wdwait.until(ExpectedConditions.elementToBeClickable(locator));
		element.clear();
		element.sendKeys(address);
	}
	protected void waitForCheckoutCountChange(WebElement elem, String text){
	    wdwait.until(ExpectedConditions.textToBePresentInElement(elem, text));
    }

	protected List<WebElement> elementsOnThePage(By locator){
	    return wd.findElements(locator);
    }
}

package com.data;

public class Contact {
	String firstName;
	String lastName;
	String address;
	String city;
	String provice;
	String country;
	String province;
	String email;
	String phone;
	String postalCode;

	public Contact(){
		this.firstName="Super";
		this.lastName="Tester";
		this.address="123 Test St";
		this.city="Aurora";
		this.province="Ontario";
		this.country="Canada";
		this.email="noneexistantxxx444@damailersz.com";
		this.phone="4164164166";
		this.postalCode="M2J9C8";
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getProvince() {
		return province;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

    public String getPostalCode() {
        return postalCode;
    }
}
